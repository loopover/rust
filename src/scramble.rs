use super::{scramble, Board, Move, Axis};
use rand::prelude::*;

pub fn scramble_board<B: Board>(board: &mut B) {
    for _ in 0..20 {
        scramble::random_state(board);
        if board.is_solvable() && !board.is_solved() { return }
    }
    panic!("Couldn't scramble board");
}

/**
 * Generate random state for board.
 * Can create unsolveable permutations for uneven board sizes.
 */
pub fn random_state<B: Board>(board: &mut B) {
    let mut indices = (0..board.cols() * board.rows()).collect::<Vec<_>>();
    indices.shuffle(&mut thread_rng());
    for row in 0..board.rows() as usize {
        for col in 0..board.cols() as usize {
            board.set(col, row, indices[row * board.cols() as usize + col]);
        }
    }
}

/**
 * Avoids moves that undo actions of previous moves and same axis moves
 * for more that 2 times in a row.
 */
pub fn smart_moves<B: Board>(board: &B, length: usize) -> Vec<Move> {
    let mut moves: Vec<Move> = Vec::new();
    for _ in 0..length {
        'outer: loop {
            let axis = *[Axis::Col, Axis::Row].choose(&mut thread_rng()).unwrap();
            let mov = Move::new(
                axis,
                random::<isize>() % if axis == Axis::Row { board.rows() } else { board.cols() } as isize,
                random::<isize>() % (if axis == Axis::Row { board.cols() } else { board.rows() } as isize - 1) + 1
            );
            let mut same_axis_in_a_row = 1;
            for last in moves.iter().rev() {
                if last.i == mov.i || same_axis_in_a_row == 2 { continue 'outer }
                if last.axis != mov.axis { break }
                same_axis_in_a_row += 1;
            }
            moves.push(mov); break
        }
    }
    moves
}

/**
 * Generates completely random axes, row / column indices and move amounts.
 */
pub fn random_moves<B: Board>(board: &B, length: usize) -> Vec<Move> {
    let mut moves: Vec<Move> = Vec::new();
    for _ in 0..length {
        let axis = *[Axis::Col, Axis::Row].choose(&mut thread_rng()).unwrap();
        let mov = Move::new(
            axis,
            random::<isize>() % if axis == Axis::Row { board.rows() } else { board.cols() } as isize,
            random::<isize>() % (if axis == Axis::Row { board.cols() } else { board.rows() } as isize - 1) + 1
        );
        moves.push(mov);
    }
    moves
}