use super::*;

pub trait Board: Clone + PartialEq + Eq {
    fn cols(&self) -> usize;
    fn rows(&self) -> usize;
    fn is_solved(&self) -> bool;
    fn is_solvable(&self) -> bool {
        if (self.cols() * self.rows()) % 2 == 0 { return true }
        let mut indices = Vec::new();
        for row in 0..self.rows() { for col in 0..self.cols() { indices.push(self.get(col, row)) } }

        let mut inversions = 0;
        for i in 0..indices.len() {
            for j in i + 1..indices.len() {
                if indices[i] > indices[j] {
                    inversions += 1;
                }
            }
        }
        inversions % 2 == 0
    }
    fn move_row(&mut self, index: usize, n: usize);
    fn move_col(&mut self, index: usize, n: usize);
    fn move_forward(&mut self, mov: Move) {
        let rows = self.rows();
        let cols = self.cols();
        match mov.axis {
            Axis::Row => self.move_row((mov.i + rows as isize) as usize % rows, (mov.n + cols as isize) as usize % cols),
            Axis::Col => self.move_col((mov.i + cols as isize) as usize % cols, (mov.n + rows as isize) as usize % rows),
        }
    }
    fn move_reverse(&mut self, mov: Move) { self.move_forward(mov.reverse()) }
    fn set(&mut self, col: usize, row: usize, index: usize);
    fn get(&self, col: usize, row: usize) -> usize;
    fn find(&self, index: usize) -> (usize, usize);
}

#[derive(Clone, PartialEq, Eq)]
pub struct BoardN {
    cols: u16, rows: u16,
    pub grid: Vec<Vec<u16>>
}

impl BoardN {
    pub fn new(cols: u16, rows: u16) -> Self {
        Self {
            cols, rows,
            grid: (0..rows).map(|y| (0..cols).map(|x| y * cols + x).collect()).collect()
        }
    }
}

impl Board for BoardN {
    fn cols(&self) -> usize { self.cols as usize }
    fn rows(&self) -> usize { self.rows as usize }
    fn is_solved(&self) -> bool {
        for (r, row) in self.grid.iter().enumerate() {
            for (c, tile) in row.iter().enumerate() {
                if *tile as usize != r * self.cols as usize + c { return false }
            }
        }
        true
    }

    fn move_row(&mut self, i: usize, n: usize) {
        let row = self.grid[i].clone();
        for (col, tile) in self.grid[i].iter_mut().enumerate() {
            *tile = row[(self.cols as usize + col - n) % self.cols as usize];
        }
    }

    fn move_col(&mut self, i: usize, n: usize) {
        let col = (0..self.rows as usize).map(|j| self.grid[j][i]).collect::<Vec<_>>();
        for row in 0..self.rows as usize {
            self.grid[row as usize][i] = col[((self.rows as usize + row) - n) % self.rows as usize];
        }
    }

    fn set(&mut self, col: usize, row: usize, index: usize) {
        self.grid[row][col] = index as u16;
    }

    fn get(&self, col: usize, row: usize) -> usize {
        self.grid[row][col] as usize
    }

    fn find(&self, index: usize) -> (usize, usize) {
        for r in 0..self.rows as usize {
            for c in 0..self.cols as usize {
                if self.grid[r][c] == index as u16 { return (c, r) }
            }
        }
        panic!()
    }
}

impl std::fmt::Debug for BoardN {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        for (r, row) in self.grid.iter().enumerate() {
            write!(f, "{}", if r > 0 { "\n\n" } else { "\n" })?;
            for i in row {
                write!(f, "{:5}", i + 1)?;
            }
        }
        write!(f, "\n")
    } 
}


#[macro_export]
macro_rules! board {
    ($name: ident, $cols: expr, $rows: expr) => {
        #[derive(Clone, PartialEq, Eq, Hash)]
        pub struct $name {
            pub grid: [[u8; $cols]; $rows]
        }

        impl $name {
            pub fn new() -> Self {
                let mut grid = [[0; $cols]; $rows];
                for r in 0..$rows { for c in 0..$cols {
                    grid[r][c] = (r * $cols + c) as u8;
                } }
                Self { grid }
            }
        }

        impl std::fmt::Debug for $name {
            fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
                for (r, row) in self.grid.iter().enumerate() {
                    write!(f, "{}", if r > 0 { "\n\n" } else { "\n" })?;
                    for i in row {
                        write!(f, "{:5}", i + 1)?;
                    }
                }
                write!(f, "\n")
            } 
        }

        impl Board for $name {
            fn cols(&self) -> usize { $cols }
            fn rows(&self) -> usize { $rows }
            fn is_solved(&self) -> bool {
                for (r, row) in self.grid.iter().enumerate() {
                    for (c, tile) in row.iter().enumerate() {
                        if *tile as usize != r * $cols + c { return false }
                    }
                }
                true
            }

            fn move_row(&mut self, i: usize, n: usize) {
                let row = self.grid[i].clone();
                for (col, tile) in self.grid[i].iter_mut().enumerate() {
                    *tile = row[($cols + col - n) % $cols];
                }
            }

            fn move_col(&mut self, i: usize, n: usize) {
                let col = (0..$rows).map(|j| self.grid[j][i]).collect::<Vec<_>>();
                for row in 0..$rows {
                    self.grid[row][i] = col[($rows + row - n) % $rows];
                }
            }

            fn set(&mut self, col: usize, row: usize, index: usize) {
                self.grid[row][col] = index as u8;
            }

            fn get(&self, col: usize, row: usize) -> usize {
                self.grid[row][col] as usize
            }

            fn find(&self, index: usize) -> (usize, usize) {
                for r in 0..$rows {
                    for c in 0..$cols {
                        if self.grid[r][c] == index as u8 { return (c, r) }
                    }
                }
                panic!()
            }
        }
    };
}

board!(Board3, 3, 3);
board!(Board4, 4, 4);
board!(Board5, 5, 5);
board!(Board4x3, 4, 3);
board!(Board4x2, 5, 3);
board!(Board5x2, 5, 2);
board!(Board5x3, 5, 3);
