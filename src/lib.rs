extern crate rand;

pub mod board;
pub use board::*;
pub mod scramble;
pub use scramble::scramble_board;
use std::fmt;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum Axis {
    Col,
    Row
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Move {
    pub axis: Axis,
    pub i: isize,
    pub n: isize
}

impl Move {
    pub fn new(axis: Axis, i: isize, n: isize) -> Move {
        Move { axis, i, n }
    }

    pub fn reverse(self) -> Move {
        Move { axis: self.axis, i: self.i, n: -self.n }
    }

    pub fn format_moves(moves: &[Move]) -> String {
        format!("{}", moves.iter().map(|mov| format!("{}", mov)).collect::<Vec<_>>().join(" "))
    }
}

impl fmt::Display for Move {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}{}{}", self.n, if self.axis == Axis::Col { "C" } else { "R" }, self.i)
    }
}
