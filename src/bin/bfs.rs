extern crate loopover;

use loopover::*;
use std::collections::VecDeque;


fn main() {
    let mut scrambled = Board3::new();
    scramble_board(&mut scrambled);
    let solved = Board3::new();

    println!("{:?}", scrambled);

    let mut queue = VecDeque::new();
    queue.push_back((scrambled, vec![]));

    while !queue.is_empty() {
        let current = queue.pop_front().unwrap();

        for (board, mov) in get_next_boards(current.0) {
            let mut path = current.1.clone();
            path.push(mov);
            if board == solved {
                println!("{}", Move::format_moves(&path));
                return
            }
            queue.push_back((board, path))
        }
    }
}

fn get_next_boards(board: Board3) -> Vec<(Board3, Move)> {
    let mut boards = Vec::new();
    for &axis in &[Axis::Col, Axis::Row] {
        for i in 0..3 {
            for n in 1..3 {
                let mut board = board.clone();
                let mov = Move::new(axis, i, n);
                board.move_forward(mov);
                boards.push((board, mov));
            }
        }
    }
    boards
}